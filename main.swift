//
// main.swift
// bpmfinder
//
// Created by Timberwolf on 2019-05-27
//

import Foundation

var inputManager = InputManager()

inputManager.setRawMode()

let quitFunc = {
	// quit
	print() // get on a separate line
	exit(0)
}
inputManager.inputActions["q"] = quitFunc
inputManager.inputActions["EOF"] = quitFunc

var bpmFinder = BPMFinder()

bpmFinder.attach(to: inputManager)

print("Press keys to measure the BPM of your keypresses!")
print("1-9: count last n. 0: count all.")
print("Press Q to quit.")

inputManager.startListening()
// bpmFinder.startFindingBPM(interval: 10_000) // interval is in µs

// sleep forever

while (true) {
	sleep(1)
}
