# BPMFinder

Find the BPM of your keypresses!

## Compilation

You'll need Swift. You can get it from https://swift.org.

After Swift is installed, just run `make` to compile it.

## Usage

```
bpmfinder
```

Then start pressing keys to measure your BPM. Avoid pressing escape sequences, such as Alt-[key] or function keys, as those will be read as multiple keypresses and mess up the estimation.
Press 1-9 to switch how many samples are averaged together, or 0 to average all of them.

## License

This is free and unencumbered software released into the public domain.
See [COPYING.md](COPYING.md) for more information.
