//
// BPMFinder.swift
// bpmfinder
//
// Created by Timberwolf on 2019-05-27
//
// Finds BPM of arbitrary events.
//

import Foundation

class BPMFinder {
	
	var averageWindow: Int = 4
	private var timingList: [Date] = []
	private var dispatchQueue: DispatchQueue? = nil

	convenience init(averageWindow: Int) {
		self.init()
		self.averageWindow = averageWindow
	}
	
	func attach(to inputManager: InputManager) {
		inputManager.inputActions["ANY - find BPM"] = { [unowned self] in
			self.timingList.append(Date()) // Date() returns now
			self.findBPM()
		}
		for n in 0...9 {
			inputManager.inputActions["\(n)"] = { [unowned self] in
				self.averageWindow = n
				self.findBPM() // update display
			}
		}
	}
	
	func findBPM() {
		guard timingList.count >= 2 else {
			return
		}
		
		let times = timingList
		// get times between all the dates
		var timings: [TimeInterval] = []
		for i in 0..<times.count-1 {
			let t1 = times[i]
			let t2 = times[i+1]
			timings.append(t2.timeIntervalSince(t1))
		}
		// averageWindow 0 means all
		if (averageWindow != 0 && times.count > averageWindow) {
			// grab tail end of the timing list
			timings = Array(timings[(timings.count - averageWindow)...])
		}
		
		
		// find the average
		var average: TimeInterval = 0
		for t in timings {
			average += t
		}
		average /= Double(timings.count)
		
		let bpm = 60.0 / average
		
		// \u{1B}K: \e[K clears to end of line without a messy string of spaces
		let lastN = (averageWindow == 0) ? "all" : "last \(averageWindow)"
		print(String(format: "\r\u{1B}[K[\(lastN)] %02gs, %01g BPM", average, bpm), terminator: "")
	}
	
	func startFindingBPM(interval: Int) {
		if (dispatchQueue == nil) {
			dispatchQueue = DispatchQueue(
				label: "Input",
				qos: .userInteractive,
				target: DispatchQueue.global(qos: .background) // help avoid excessive thread creation - see https://developer.apple.com/documentation/dispatch/dispatchqueue/
			)
		}
		dispatchQueue?.async { [unowned self] in
			while (true) {
				self.findBPM()
				usleep(UInt32(interval))
			}
		}
	}
	
}
