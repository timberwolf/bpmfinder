//
// InputManager.swift
// bpmfinder
//
// Created by Timberwolf on 2019-05-27
//
// Handles keyboard input.
//

import Foundation

class InputManager {
	
	var inputActions: [String: (() -> ())] = [:]
	
	private var dispatchQueue: DispatchQueue? = nil
	private var originalTermSettings: termios? = nil
	
	func setRawMode() {
		// turn off terminal input handling
		// see https://github.com/chjj/pty.js/issues/18
		// see https://stackoverflow.com/questions/31465943/using-termios-in-swift
		var termsettings = termios()
		tcgetattr(STDIN_FILENO, &termsettings)
		
		originalTermSettings = termsettings
		
		termsettings.c_lflag &= ~(UInt32(ICANON))
		termsettings.c_lflag &= ~(UInt32(ECHO))
		tcsetattr(STDIN_FILENO, TCSANOW, &termsettings)
	}
	
	func startListening() {
		if (dispatchQueue == nil) {
			dispatchQueue = DispatchQueue(
				label: "Input",
				qos: .userInteractive,
				target: DispatchQueue.global(qos: .userInteractive) // help avoid excessive thread creation - see https://developer.apple.com/documentation/dispatch/dispatchqueue/
			)
		}
		dispatchQueue?.async { [unowned self] in
			while (true) {
				let cAsInt = getchar()
				if cAsInt == EOF {
					// run the EOF function, if any
					self.inputActions["EOF"]?()
					continue
				}
				let c = UInt32(cAsInt)
				guard let u = UnicodeScalar(c) else {
					fputs("*** Couldn't convert input to a unicode scalar! ***\n", stderr)
					exit(1)
				}
				let char = Character(u)
				let str = String(char)
				if (self.inputActions.keys.contains(str)) {
					self.inputActions[str]!()
				}
				let anys = self.inputActions.keys.filter { $0.hasPrefix("ANY") }
				let nonAnys = self.inputActions.keys.filter { !$0.hasPrefix("ANY") }
				// limit ANY to keys that haven't been otherwise assigned
				if (!nonAnys.contains(str)) {
					for any in anys {
						self.inputActions[any]!()
					}
				}
			}
		}
	}
	
}
