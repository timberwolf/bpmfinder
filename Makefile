#
# Makefile for bpmfinder
# vim:set fo=tcqr:
#

bpmfinder: *.swift
	swiftc -g -o bpmfinder *.swift

release: *.swift
	mkdir -p dist/
	swiftc -o dist/bpmfinder *.swift

clean:
	rm -f bpmfinder
	rm -rf dist/
